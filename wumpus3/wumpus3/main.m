//
//  main.m
//  wumpus3
//
//  Created by Petros Loukareas on 21/07/2020.
//  Copyright © 2020 Petros Loukareas. All rights reserved.
//


#import <Foundation/Foundation.h>

int a;
int win;
int t[4];
bool gameOn;
bool sameSetup;
int l[8], m[8];
bool kinokoRules;
bool bumpFirstCheck = false;
bool dontEatArrow;

int cave[60] = { 1, 4, 7, 0, 2, 9, 1, 3, 11, 2, 4, 13, 0, 3, 5, 4,
                 6, 14, 5, 7, 16, 0, 6, 8, 7, 9, 17, 1, 8, 10, 9, 11,
                 18, 2, 10, 12, 11, 13, 19, 3, 12, 14, 5, 13, 15, 14,
                 16, 19, 6, 15, 7, 8, 16, 18, 10, 17, 19, 12, 15, 18 };

void moveWumpus(void);
bool isPlayerInSameRoomAsWumpus(void);
int shootArrow(void);
void reshuffleHazards(void);
void checkHazards(void);
void showHazards(void);

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        // Method declarations
        bool arrayContainsValue(int *array, int value, int elements);
        void setUpNewGame(void);
        NSString* getInput(int numberOfCharsReturned, BOOL suppressPrompt);
        void printHelp(void);

        // Scratch variables
        gameOn = true;
        sameSetup = false;
        
        // Print credits?
        if (argc > 1) {
            NSMutableArray *arguments = [[NSMutableArray alloc] init];
            for (int i = 1; i < argc; i++) {
                [arguments addObject:[NSString stringWithUTF8String:argv[i]]];
            }
            if ([arguments containsObject:@"--credits"]) {
                // Print credits and exit
                printf("\r\nCREDITS FOR WUMPUS 3                       ObjC 1.1 Build 2\r\n");
                printf("-----------------------------------------------------------\r\n");
                printf("Original concept and program: Gregory Yob            (1973)\r\n");
                printf("  (published in People's Computer Company newsletter, 1974)\r\n");
                printf("Further modifications:        Howie Franklin (year unknown)\r\n");
                printf("Objective-C port:             Kinoko House           (2020)\r\n");
                printf("          (also, some gameplay additions -  use option -kr)\r\n");
                printf("\r\n");
                printf("The source to this port is released into the public domain:\r\n");
                printf("https://bitbucket.org/kinokohouse/wumpus\r\n\r\n");
                return 0;
            }
            if ([arguments containsObject:@"-kr"] || [arguments containsObject:@"--kinokorules"]) {
                kinokoRules = true;
                printf("Kinoko House rules selected -- good choice!\r\n");
            } else if ([arguments containsObject:@"-h"] || [arguments containsObject:@"--help"]) {
                printHelp();
                return 0;
            } else {
                printf("Unknown option(s) -- ignored.\r\n");
            }
        }
        
        // Game start
        printf("\r\n                               WUMPUS 3\r\n");
        printf("               PEOPLE'S COMPUTER COMPANY, MENLO PARK, CA\r\n\r\n\r\n");
        printf("Welcome to 'Wumpus 3'!\r\nNeed instructions (y/N)? ");
        NSString *n = getInput(1, true);
        if ([n isEqualToString:@"y"]) printHelp();

        // Game loop
        while (gameOn) {
            if (!sameSetup) {
                setUpNewGame();
            }
            a = 5;
            dontEatArrow = false;
            printf("\r\nHunt the Wumpus!\r\n");
            win = 0;
            if (kinokoRules) bumpFirstCheck = true;
            while (win == 0) {
                if (kinokoRules) {
                    if (!bumpFirstCheck) {
                        reshuffleHazards();
                    } else {
                        bumpFirstCheck = false;
                    }
                } else {
                    reshuffleHazards();
                }
                checkHazards();
                if (win == -1) goto gameEnd;
                showHazards();
                printf("You are in room %d.\r\n",t[0] + 1);
                printf("Tunnels lead to rooms %d, %d and %d.\r\n\r\n", t[1] + 1, t[2] + 1, t[3] + 1);
                NSString *answer = @"";
                while ([answer isNotEqualTo:@"s"] && [answer isNotEqualTo:@"m"]) {
                    printf("Shoot or move (s/m)? ");
                    answer = getInput(1, true);
                }
                if ([answer isEqualToString:@"s"]) {
                    int result = shootArrow();
                    if (result == 0) {
                        printf("Missed...\r\n");
                    }
                    if (result == 0 || result == 2) {
                        moveWumpus();
                        if (isPlayerInSameRoomAsWumpus()) {
                            printf("Tsk tsk tsk -- Wumpus got you!\r\n");
                            win = -1;
                        } else {
                            a--;
                            if (a == 0) {
                                printf("Out of arrows!\r\n");
                                win = -1;
                            }
                        }
                    } else if (result == 2) {
                        win = 0;
                    } else if (result == 3) {
                        win = -1;
                    } else {
                        if (result == -1) printf("OUCH! Arrow got you!\r\n");
                        win = result;
                    }
                } else if ([answer isEqualToString:@"m"]) {
                    int newRoom = -1;
                    while (newRoom != t[1] + 1 && newRoom != t[2] + 1 && newRoom != t[3] + 1) {
                        printf("Where to? ");
                        newRoom = [getInput(0, true) intValue];
                        if (newRoom != t[1] + 1 && newRoom != t[2] + 1 && newRoom != t[3] + 1) {
                            if (newRoom < 1 || newRoom > 20) {
                                printf("No such room - try another one.\r\n");
                            } else {
                                printf("Not possible - please choose between %d, %d and %d.\r\n", t[1] + 1, t[2] + 1, t[3] + 1);
                            }
                        }
                    }
                    l[0] = newRoom - 1;
                }
            }
        gameEnd:
            if (win == -1) {
                printf("Ha ha ha - You lose!");
            } else {
                printf("Aha! you got the Wumpus!\r\n");
                printf("Hee hee hee - The Wumpus'll getcha next time!!");
            }
            printf("\r\n\r\nAnother game (Y/n)? ");
            NSString *aw = getInput(1, true);
            if ([aw isEqualToString:@"y"] || [aw isEqualToString:@""]) {
                gameOn = true;
                printf("Same setup (y/N)? ");
                if ([getInput(1, true) isEqualToString:@"y"]) {
                    sameSetup = true;
                    for (int i = 0; i < 6; i++) {
                        l[i] = m[i];
                    }
                } else {
                    sameSetup = false;
                }
            } else {
                gameOn = false;
            }
        }
    }
    printf("Thanks for playing, see you again!\r\n");
    return 0;
}

bool arrayContainsValue(int *array, int value, int elements) {
    for (int i = 0; i < elements; i++) {
        if (array[i] == value) return true;
    }
    return false;
}

void setUpNewGame() {
    for (int i = 0; i < 7; i++) {
        int v = arc4random_uniform(19);
        while (arrayContainsValue(l, v, i)) {
            v = arc4random_uniform(19);
        }
        l[i] = v;
        m[i] = v;
    }
    if (kinokoRules) {
        int q = arc4random_uniform(19);
        if (q >= 10) {
            q = arc4random_uniform(19);
            while (arrayContainsValue(l, q, 7)) {
                q = arc4random_uniform(19);
            }
            l[7] = q;
            m[7] = q;
        } else {
            l[7] = 99;
            m[7] = 99;
        }
    }
}

void reshuffleHazards() {
    int r;
    r = arc4random_uniform(11);
    if (r == 0) {
        printf("\r\nDon't blink now, but I hear the Wumpus sleep-walking!!\r\n");
        l[1] = arc4random_uniform(19);
        if (l[0] == l[1]) {
            printf("...Oh drat, he sleep-walked right into your room!\r\n");
            return;
        }
    }
    r = arc4random_uniform(11);
    if (r == 0) {
        printf("\r\nRUMBLE RUMBLE - You're standing on shaky ground...\r\n");
        printf("The old pits have closed and new ones have formed!\r\n");
        l[2] = arc4random_uniform(19);
    reshufflePit:
        l[3] = arc4random_uniform(19);
        if (l[2] == l[3]) goto reshufflePit;
        if (l[0] == l[2] || l[0] == l[3]) {
            printf("...WHOA! The ground just split open right below your feet!\r\n");
            return;
        }
    }
    r = arc4random_uniform(11);
    if (r == 0) {
        printf("\r\nWhat a flap you're in... It's bat migration time!\r\n");
        l[4] = arc4random_uniform(19);
    reshuffleBat:
        l[5] = arc4random_uniform(19);
        if (l[4] == l[5]) goto reshuffleBat;
        if (l[0] == l[4] || l[0] == l[5]) {
            printf("...Oh no, they flew right into *your* room!\r\n");
        }
    }
    r = arc4random_uniform(11);
    if (r == 0) {
        printf("\r\nBUZZ BUZZ - The tumaeros are swarming.\r\n");
        l[6] = arc4random_uniform(19);
        if (l[0] == l[6]) {
            printf("...Aaaaannd they're in *your* room now!\r\n");
        }
        if (kinokoRules) {
            if (l[6] == l[7]) {
                if (l[7] != 99) {
                    l[7] = 99;
                    printf("In the distance, you hear tumaeros feasting on wood...\r\n");
                }
            }
        }
    }
}

void showHazards() {
    printf("\r\n");
    t[0] = l[0];
    bool tflag = false;
    for (int i = 1; i < 4; i++) {
        t[i] = cave[(t[0] * 3) + i - 1];
        if (l[1] == t[i]) {
            printf("I smell a Wumpus!\r\n");
            tflag = true;
        }
        if (l[2] == t[i]) {
            printf("I feel a draft!\r\n");
            tflag = true;
        }
        if (l[3] == t[i]) {
            printf("I feel a draft!\r\n");
            tflag = true;
        }
        if (l[4] == t[i]) {
            printf("Bats nearby!\r\n");
            tflag = true;
        }
        if (l[5] == t[i]) {
            printf("Bats nearby!\r\n");
            tflag = true;
        }
        if (l[6] == t[i]) {
            printf("My arrows are quivering!\r\n");
            tflag = true;
        }
    }
    if (tflag) printf("\r\n");
}

void checkHazards() {
recheckHazards:
    if (l[0] == l[1]) {
        printf("Oops! Bumped into a Wumpus!\r\n");
        moveWumpus();
        if (isPlayerInSameRoomAsWumpus()) {
            printf("...And now the Wumpus has bumped into you!\r\n");
            printf("Tsk tsk tsk -- Wumpus got you!\r\n");
            win = -1;
            return;
        }
    }
    if (l[0] == l[2] || l[0] == l[3]) {
        printf("YYYIIIIEEEE . . . Fell in pit.\r\n");
        win = -1;
        return;
    }
    if (l[0] == l[4]) {
        printf("ZAP -- Super bat snatch! Elsewheresville for you!\r\n");
        l[0] = arc4random_uniform(19);
        goto recheckHazards;
    }
    if (l[0] == l[5]) {
        printf("ZAP -- Super bat snatch! Elsewheresville for you!\r\n");
        l[0] = arc4random_uniform(19);
        goto recheckHazards;
    }
    if (l[0] == l[6]) {
        if (!dontEatArrow) {
            printf("CHOMP CHOMP -- That was one tasty arrow!\r\n");
            a--;
            if (a < 1) {
                printf("Tumaeros ate your last arrow!\r\n");
                win = -1;
                return;
            } else {
                printf("You have %d arrows left.\r\n", a);
            }
        } else {
            dontEatArrow = false;
        }
    }
    if (kinokoRules) {
        if (l[0] == l[7]) {
            printf("\r\nOh goody, free arrow! Don't mind if I do...\r\n");
            a++;
            l[7] = 99;
            printf("You now have %d arrows.\r\n", a);
        }
    }
    win = 0;
}

NSString* getInput(int numberOfCharsReturned, BOOL suppressPrompt) {
    char input[1000] = { };
    if (!suppressPrompt) printf("> ");
    fgets(input, sizeof(input), stdin);
    NSString *i = [[NSString stringWithUTF8String:input] lowercaseString];
    i = [[i componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
    if ([i length] == 0) return @"";
    if (numberOfCharsReturned < 1) {
        return i;
    } else {
        if ([i length] >= numberOfCharsReturned) {
            return [i substringWithRange:NSMakeRange(0, numberOfCharsReturned)];
        } else {
            return i;
        }
    }
}

void moveWumpus() {
    printf("\r\nWumpus woke up and went on the move...\r\n");
    int mvarray[4] = { };
    int wroom = l[1];
    mvarray[0] = wroom;
    for (int j = (wroom * 3); j < ((wroom * 3) + 3); j++) {
        mvarray[j - (wroom * 3) + 1] = cave[j];
    }
    l[1] = mvarray[arc4random_uniform(3)];
}

bool isPlayerInSameRoomAsWumpus() {
    if (l[0] == l[1]) return true;
    return false;
}

int shootArrow() {
    if (kinokoRules) {
        if (l[0] == l[6]) {
            printf("The tumaeros swarm your bow as you lay on the arrow... CHOMP CHOMP!\r\n");
            printf("Better move to another room first methinks!\r\n");
            dontEatArrow = true;
            a--;
            if (a == 0) {
                printf("Aaannnndd... That was your last arrow.\r\n");
                return 3;
            }
            return 2;
        }
    }
    int numberOfRooms = 0;
    int t[3];
    while (numberOfRooms < 1 || numberOfRooms > 5) {
        printf("How many rooms (1 - 5)? ");
        numberOfRooms = [getInput(0, true) intValue];
        if (numberOfRooms > 5) {
            printf("Arrow don't travel *that* far!\r\n");
        }
        if (numberOfRooms < 1) {
            printf("A number between 1 and 5 will do.\r\n");
        }
    }
    int arrows[numberOfRooms];
    int pf;
    for (int i = 0; i < numberOfRooms; i++) {
        pf = 0;
        while (pf < 1 || pf > 20) {
            printf("Room #%d? ", i + 1);
            pf = [getInput(0, true) intValue];
            if (pf < 1 || pf > 20) {
                printf("Illegal room number -- please try again.\r\n");
            }
        }
        pf--;
        if (i > 1) {
            if (pf == arrows[i - 2]) {
                printf("Arrows aren't *that* crooked -- try another room.\r\n");
                i--;
                continue;
            } else {
                arrows[i] = pf;
            }
        } else {
            arrows[i] = pf;
        }
    }
    int l0 = l[0];
    if (l0 == arrows[0]) return -1;
    for (int i = 0; i < numberOfRooms; i++) {
        for (int j = 0; j < 3; j++) {
            t[j] = cave[(l0 * 3) + j];
        }
        int p = -1;
        for (int k = 0; k < 3; k++) {
            if (t[k] == arrows[i]) {
                l0 = t[k];
                p = l0;
                break;
            }
        }
        if (p == -1) {
            l0 = t[arc4random_uniform(2)];
            p = l0;
        }
        if (kinokoRules) {
            if (p == l[6]) {
                printf("Chomp chomp - arrow was eaten by tumaeros while in flight!\r\n");
                return 2;
            }
        }
        if (p == l[0]) {
            return -1;
        }
        if (p == l[1]) {
            return 1;
        }
    }
    return 0;
}

void printHelp() {
    printf("\r\nThis version (wumpus3) of 'Hunt the Wumpus' is played\r\n");
    printf("like the 'normal' version with a few (chuckle) additions:\r\n\r\n");
    printf("TUMAERO   (anaerobic termite) swarm:  eats crooked arrows,\r\n");
    printf("          one arrow each time you enter its room.\r\n\r\n");
    printf("WARNING:  'My arrows are quivering!' when you are one room away.\r\n\r\n");
    printf("HAZARDS CAN MOVE!!\r\n");
    printf("     WUMPUS   - The Wumpus sleep-walks\r\n");
    printf("     PITS     - Earthquakes close the old pits and form new ones\r\n");
    printf("     BATS     - Bat migration\r\n");
    printf("     TUMAEROS - The tumaeros swarm in search of food\r\n\r\n");
    printf("HOUSE RULES\r\n");
    printf("You can also elect to play the game by Kinoko House's rules by\r\n");
    printf("starting the game with the --kinokorules or -kr option. This will\r\n");
    printf("make some things a bit more difficult:\r\n\r\n");
    printf("     - If you choose to guide your arrow through multiple rooms, and if\r\n");
    printf("       it happens to pass the room where the tumaero swarm is in, THEY\r\n");
    printf("       WILL EAT YOUR ARROW WHILE IT IS IN FLIGHT!\r\n");
    printf("     - Speaking of tumaeros... If you happen onto them and get an arrow\r\n");
    printf("       eaten by them, and then make the mistake of staying in that room and\r\n");
    printf("       actually drawing you bow there, your arrow will be eaten from\r\n");
    printf("       off the bow.\r\n\r\n");
    printf("You will however get the following boons:\r\n\r\n");
    printf("     - In the original game, the hazards could start moving around at\r\n");
    printf("       the very start, so you could get jumped immediately by a Wumpus\r\n");
    printf("       without having even made a single move! In KC rules, this can't\r\n");
    printf("       happen during the first turn. (The Wumpus will get you anyway the\r\n");
    printf("       next round, hee hee!)\r\n");
    printf("     - There's a 50 per cent chance the game starts with an arrow lying\r\n");
    printf("       around in a room somewhere, left by another hunter, waiting for\r\n");
    printf("       you to get picked up. Just make sure you get there before the\r\n");
    printf("       tumaeros do... Chomp chomp!\r\n\r\n");
    printf( "Good luck!\r\n\r\n");
}
