//
//  main.m
//  wumpus2
//
//  Created by Petros Loukareas on 20/07/2020.
//  Copyright © 2020 Petros Loukareas. All rights reserved.
//


#import <Foundation/Foundation.h>
int a;
int win;
int t[4];
int l[6], m[6];
bool gameOn;
bool sameSetup;
bool setNewCave;

int *cave;

// Cave data: Dodecahedron
int dodc[60] = { 1, 4, 7, 0, 2, 9, 1, 3, 11, 2, 4, 13,
                 0, 3, 5, 4, 6, 14, 5, 7, 16, 0, 6, 8,
                 7, 9, 17, 1, 8, 10, 9, 11, 18, 2, 10, 12,
                 11, 13, 19, 3, 12, 14, 5, 13, 15, 14, 16, 19,
                 6, 15, 7, 8, 16, 18, 10, 17, 19, 12, 15, 18 };

// Cave data: Moebius strip
int moeb[60] = { 19, 1, 2, 18, 0, 3, 0, 3, 4, 1, 2, 5,
                 2, 5, 6, 3, 4, 7, 4, 7, 8, 5, 6, 9,
                 6, 9, 10, 7, 8, 11,  8, 11, 12, 9, 10, 13,
                 10, 13, 14, 11, 12, 15, 11, 15, 16, 13, 14, 17,
                 14, 17, 18, 15, 16, 19, 1, 16, 19, 0, 17, 18 };

// Cave data: String of beads
int bead[60] = { 1, 2, 19, 0, 2, 3, 0, 1, 3, 1, 2, 4,
                 3, 5, 6, 4, 6, 7, 4, 5, 8, 5, 6, 8,
                 7, 9, 10, 8, 10, 11, 8, 9, 11, 9, 10, 12,
                 11, 13, 14, 12, 14, 15, 12, 13, 15, 13, 14, 16,
                 15, 17, 18, 16, 18, 19, 16, 17, 19, 0, 17, 18 };

// Cave data: Hex nut on torus
int hxto[60] = { 5, 9, 15, 5, 6, 16, 6, 7, 17, 7, 8, 18,
                 8, 9, 19, 0, 1, 14, 1, 2, 10, 2, 3, 11,
                 3, 4, 12, 4, 5, 13, 6, 15, 19, 7, 15, 16,
                 8, 16, 17, 9, 17, 18, 5, 18, 19, 0, 10, 11,
                 1, 11, 12, 2, 12, 13, 3, 13, 14, 4, 10, 14 };

// Cave data: Dendrite w/ degeneracies
int dend[60] = { 0, 0, 4, 1, 1, 4, 2, 2, 5, 3, 3, 5,
                 0, 1, 6, 2, 3, 6, 4, 5, 9, 7, 8, 8,
                 7, 7, 9, 6, 8, 10, 9, 12, 13, 11, 12, 12,
                 10, 11, 11, 10, 14, 15, 13, 16, 17, 13, 18, 19,
                 14, 16, 16, 14, 17, 17, 15, 18, 18, 15, 19, 19 };

// Cave data: One way lattice
int onew[60] = { 4, 3, 7, 0, 4, 5, 1, 5, 6, 2, 6, 7,
                 7, 8, 11, 4, 8, 9, 5, 9, 10, 6, 10, 11,
                 11, 12, 15, 8, 12, 13, 9, 13, 14, 10, 14, 15,
                 15, 16, 19, 12, 16, 17, 13, 17, 18, 14, 18, 19,
                 0, 3, 19, 0, 1, 16,  1, 2, 17, 2, 3, 18 };
    
// Cave data: BYOC (Bring Your Own Cave)
int byoc[60] = { };


int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        // Method declarations
        bool arrayContainsValue(int *array, int value, int elements);
        void setUpNewGame(void);
        NSString* getInput(int numberOfCharsReturned, bool suppressPrompt);
        void printHelp(void);
        
        // Game related routines
        void moveWumpus(void);
        bool isPlayerInSameRoomAsWumpus(void);
        int shootArrow(void);
        void editYourOwnCave(void);
        
        // Scratch variables
        gameOn = true;
        sameSetup = false;
        setNewCave = true;
        
        // Print credits?
        if (argv[1] != NULL) {
            NSString *arg = [NSString stringWithUTF8String:argv[1]];
            if ([arg isEqualToString:@"--credits"]) {
                
                // Print credits and exit
                printf("\r\nCREDITS FOR WUMPUS II                     ObjC 1.01 Build 3\r\n");
                printf("-----------------------------------------------------------\r\n");
                printf("Original concept and program: Gregory Yob            (1973)\r\n");
                printf("                  (published in Creative Computing in 1977)\r\n");
                printf("Objective-C port:             Kinoko House           (2020)\r\n");
                printf("\r\n");
                printf("The source to this port is released into the public domain:\r\n");
                printf("https://bitbucket.org/kinokohouse/wumpus\r\n\r\n");
            }
            return 0;
        }
        
        // Game start
        printf("\r\n                         WUMPUS 2\r\n");
        printf("                    CREATIVE COMPUTING\r\n");
        printf("                  MORRISTOWN, NEW JERSEY\r\n\r\n\r\n");
        printf("Welcome to 'Wumpus II'!\r\n\r\nNeed instructions/explanation of cave types (y/N)? ");
        NSString *n = getInput(1, true);
        if ([n isEqualToString:@"y"]) printHelp();
        
        // Game loop
        while (gameOn) {
            if (setNewCave) {
                sameSetup = false;
                NSString *awr = @"!";
                NSArray *caveChoice = @[@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"h",@""];
            rechooseCave:
                while (![caveChoice containsObject:awr]) {
                    printf("Which cave ([0] - 6, h = help)? ");
                    awr = getInput(1, true);
                    if ([awr isEqualToString:@""]) {
                        awr = @"0";
                        printf("Ah, the usual then, eh?\r\n");
                    } else if ([awr isEqualToString:@"h"]) {
                        printHelp();
                        awr = @"!";
                        goto rechooseCave;
                    } else if (![caveChoice containsObject:awr]) {
                        printf("Illegal input -- once more, with feeling... ");
                    }
                }
                int c = [awr intValue];
                switch(c) {
                    case 0:
                        cave = dodc;
                        printf("Chose the dodecahedron cave.\r\n");
                        break;
                    case 1:
                        printf("Chose the Moebius strip.\r\n");
                        cave = moeb;
                        break;
                    case 2:
                        printf("Chose the string of beads.\r\n");
                        cave = bead;
                        break;
                    case 3:
                        printf("Chose the hex nut doughnut cave.\r\n");
                        cave = hxto;
                        break;
                    case 4:
                        printf("Chose the dendrite with degeneracies.\r\n");
                        cave = dend;
                        break;
                    case 5:
                        printf("Chose the one-way lattice.\r\n");
                        cave = onew;
                        break;
                    case 6:
                        printf("Chose to edit your own cave...\r\n");
                        editYourOwnCave();
                        if (byoc[0] == 99) {
                            printf("\r\nAbandoned cave entry -- too bad, try again later!\r\n");
                            awr = @"!";
                            goto rechooseCave;
                        } else {
                            cave = byoc;
                        }
                        break;
                    default:
                        cave = dend;
                }
            }
            if (!sameSetup) {
                setUpNewGame();
            }
            a = 5;
            printf("\r\nHunt the Wumpus!\r\n");
            win = 0;
            while (win == 0) {
                printf("\r\n");
                t[0] = l[0];
                bool tflag = false;
                for (int i = 1; i < 4; i++) {
                    t[i] = cave[(t[0] * 3) + i - 1];
                    if (l[1] == t[i]) {
                        printf("I smell a Wumpus!\r\n");
                        tflag = true;
                    }
                    if (l[2] == t[i]) {
                        printf("I feel a draft!\r\n");
                        tflag = true;
                    }
                    if (l[3] == t[i]) {
                        printf("I feel a draft!\r\n");
                        tflag = true;
                    }
                    if (l[4] == t[i]) {
                        printf("Bats nearby!\r\n");
                        tflag = true;
                    }
                    if (l[5] == t[i]) {
                        printf("Bats nearby!\r\n");
                        tflag = true;
                    }
                }
                if (tflag) printf("\r\n");
                printf("You are in room %d.\r\n",t[0] + 1);
                printf("Tunnels lead to rooms %d, %d and %d.\r\n\r\n", t[1] + 1, t[2] + 1, t[3] + 1);
                NSString *answer = @"";
                while ([answer isNotEqualTo:@"s"] && [answer isNotEqualTo:@"m"]) {
                    printf("Shoot or move (s/m)? ");
                    answer = getInput(1, true);
                }
                if ([answer isEqualToString:@"s"]) {
                    int result = shootArrow();
                    if (result == 0) {
                        printf("Missed...\r\n");
                        moveWumpus();
                        if (isPlayerInSameRoomAsWumpus()) {
                            printf("Tsk tsk tsk -- Wumpus got you!\r\n");
                            win = -1;
                        } else {
                            a--;
                            if (a == 0) {
                                printf("You have used all of your arrows!\r\n");
                                win = -1;
                            }
                        }
                    } else {
                        if (result == -1) printf("OUCH! Arrow got you!\r\n");
                        win = result;
                    }
                } else if ([answer isEqualToString:@"m"]) {
                    int newRoom = -1;
                    while (newRoom != t[1] + 1 && newRoom != t[2] + 1 && newRoom != t[3] + 1) {
                        printf("Where to? ");
                        newRoom = [getInput(0, true) intValue];
                        if (newRoom != t[1] + 1 && newRoom != t[2] + 1 && newRoom != t[3] + 1) {
                            if (newRoom < 1 || newRoom > 20) {
                                printf("No such room - try another one.");
                            } else {
                                printf("Not possible - please choose between %d, %d and %d.\r\n", t[1] + 1, t[2] + 1, t[3] + 1);
                            }
                        }
                    }
                    l[0] = newRoom - 1;
                checkHazards:
                    if (l[0] == l[1]) {
                        printf("Oops! Bumped into a Wumpus!\r\n");
                        moveWumpus();
                        if (isPlayerInSameRoomAsWumpus()) {
                            printf("Tsk tsk tsk -- Wumpus got you!\r\n");
                            win = -1;
                        }
                    }
                    if (l[0] == l[2] || l[0] == l[3]) {
                        printf("YYYIIIIEEEE . . . Fell in pit.\r\n");
                        win = -1;
                    }
                    if (l[0] == l[4]) {
                        printf("ZAP -- Super bat snatch! Elsewheresville for you!\r\n");
                        l[0] =arc4random_uniform(19);
                        goto checkHazards;
                    }
                    if (l[0] == l[5]) {
                        printf("ZAP -- Super bat snatch! Elsewheresville for you!\r\n");
                        l[0] =arc4random_uniform(19);
                        goto checkHazards;
                    }
                }
            }
            if (win == -1) {
                printf("Ha ha ha - You lose!");
                
            } else {
                printf("Aha! you got the Wumpus!\r\n");
                printf("Hee hee hee - The Wumpus'll getcha next time!!");
            }
            printf("\r\n\r\nAnother game (Y/n)? ");
            NSString *aw = getInput(1, true);
            if ([aw isEqualToString:@"y"] || [aw isEqualToString:@""]) {
                gameOn = true;
                printf("Same setup (y/N, c = choose another cave)? ");
                NSString *g = getInput(1, true);
                if ([g isEqualToString:@"y"]) {
                    setNewCave = false;
                    sameSetup = true;
                    for (int i = 0; i < 6; i++) {
                        l[i] = m[i];
                    }
                } else if ([g isEqualToString:@"c"]){
                    setNewCave = true;
                    sameSetup = false;
                } else {
                    setNewCave = false;
                    sameSetup = false;
                }
            } else {
                gameOn = false;
            }
        }
    }
    printf("Thanks for playing, see you again!\r\n");
    return 0;
}

bool arrayContainsValue(int *array, int value, int elements) {
    for (int i = 0; i < elements; i++) {
        if (array[i] == value) return true;
    }
    return false;
}

void setUpNewGame() {
    for (int i = 0; i < 6; i++) {
        int v = arc4random_uniform(19);
        while (arrayContainsValue(l, v, i)) {
            v = arc4random_uniform(19);
        }
        l[i] = v;
        m[i] = v;
    }
}

NSString* getInput(int numberOfCharsReturned, bool suppressPrompt) {
    char input[1000] = { };
    if (!suppressPrompt) printf("> ");
    fgets(input, sizeof(input), stdin);
    NSString *i = [[NSString stringWithUTF8String:input] lowercaseString];
    i = [[i componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
    if ([i length] == 0) return @"";
    if (numberOfCharsReturned < 1) {
        return i;
    } else {
        if ([i length] >= numberOfCharsReturned) {
            return [i substringWithRange:NSMakeRange(0, numberOfCharsReturned)];
        } else {
            return i;
        }
    }
}

void moveWumpus() {
    int mvarray[4] = { };
    int wroom = l[1];
    mvarray[0] = wroom;
    for (int j = (wroom * 3); j < ((wroom * 3) + 3); j++) {
        mvarray[j - (wroom * 3) + 1] = cave[j];
    }
    l[1] = mvarray[arc4random_uniform(3)];
}

bool isPlayerInSameRoomAsWumpus() {
    if (l[0] == l[1]) return true;
    return false;
}

int shootArrow() {
    int numberOfRooms = 0;
    int t[3];
    while (numberOfRooms < 1 || numberOfRooms > 5) {
        printf("How many rooms (1 - 5)? ");
        numberOfRooms = [getInput(0, true) intValue];
        if (numberOfRooms > 5) {
            printf("Arrows don't travel *that* far!\r\n");
        }
        if (numberOfRooms < 1) {
            printf("Something between 1 and 5 will do nicely.\r\n");
        }
    }
    int arrows[numberOfRooms];
    int pf;
    for (int i = 0; i < numberOfRooms; i++) {
        pf = 0;
        while (pf < 1 || pf > 20) {
            printf("Room #%d? ", i + 1);
            pf = [getInput(0, true) intValue];
            if (pf < 1 || pf > 20) {
                printf("Illegal room number -- please try again.\r\n");
            }
        }
        pf--;
        if (i > 1) {
            if (pf == arrows[i - 2]) {
                printf("Arrows aren't *that* crooked -- try another room.\r\n");
                i--;
                continue;
            } else {
                arrows[i] = pf;
            }
        } else {
            arrows[i] = pf;
        }
    }
    int l0 = l[0];
    if (l0 == arrows[0]) return -1;
    for (int i = 0; i < numberOfRooms; i++) {
        for (int j = 0; j < 3; j++) {
            t[j] = cave[(l0 * 3) + j];
        }
        int p = -1;
        for (int k = 0; k < 3; k++) {
            if (t[k] == arrows[i]) {
                l0 = t[k];
                p = l0;
                break;
            }
        }
        if (p == -1) {
            l0 = t[arc4random_uniform(2)];
            p = l0;
        }
        if (p == l[0]) {
            return -1;
        }
        if (p == l[1]) {
            return 1;
        }
    }
    return 0;
}

void editYourOwnCave() {
    NSString *roomInput;
    NSArray *inputMorsels;
    bool inputRulesSatisfied;
    printf("\r\nWumpus II Cave Editor\r\n");
    printf("---------------------\r\n");
    printf("Please enter connected rooms in COMMA-SEPARATED groups of three!\r\n\r\n");
    printf("Enter a single 'x' to abort entry and return to cave choice.\r\n\r\n");
    for (int i = 0; i < 20; i++) {
        inputRulesSatisfied = false;
        while (!inputRulesSatisfied) {
            printf("Which rooms connect to room #%d? ", i + 1);
            roomInput = getInput(0, true);
            if ([roomInput isEqualToString:@"x"]) {
                byoc[0] = 99;
                return;
            }
            inputMorsels = [roomInput componentsSeparatedByString:@","];
            if ([inputMorsels count] > 2) {
                bool noMissFlag = true;
                for (int j = 0; j < 3; j++) {
                    int t = [[inputMorsels objectAtIndex:j] intValue];
                    if (t < 1 || t > 20) {
                        noMissFlag = false;
                        break;
                    }
                }
                if (!noMissFlag) {
                    printf("-- Invalid input - rooms are between 1 and 20.\r\n");
                    inputRulesSatisfied = false;
                } else {
                    inputRulesSatisfied = true;
                    if ([inputMorsels count] > 3) {
                        printf("-- Excess input ignored.\r\n");
                    }
                }
            } else {
                printf("-- Input incomplete - try again.\r\n");
                inputRulesSatisfied = false;
            }
        }
        for (int k = 0; k < 3; k++) {
            byoc[(i * 3) + k] = [[inputMorsels objectAtIndex:k] intValue] - 1;
        }
    }
    printf("\r\nCave entry complete; ready to play!\r\n\r\n");
}

void printHelp() {
    printf("\r\nWelcome to Wumpus II\r\n");
    printf("--------------------\r\n\r\n");
    printf("This version has the same rules as 'Hunt the Wumpus'. However, you now have a\r\n");
    printf("choice of caves to play in. Some caves are easier than others. All caves have\r\n");
    printf("20 rooms and 3 tunnels leading from one room to other rooms.\r\n\r\n");
    printf("The caves are:\r\n\r\n");
    printf("0    -    DODECAHEDRON: The rooms of this cave are on a 12-sided object, each\r\n");
    printf("          forming a pentagon. The rooms are at the corners of the pentagons,\r\n");
    printf("          each room having tunnels that lead to 3 other rooms.\r\n\r\n");
    printf("1    -    MOEBIUS STRIP: This cave is two rooms wide and 10 rooms around \r\n");
    printf("          (like a belt). You will notice there is a half twist somewhere.\r\n\r\n");
    printf("2    -    STRING OF BEADS: Five beads in a circle. Each bead is a diamond with\r\n");
    printf("          a vertical cross-bar. The right and left corners lead to neighboring\r\n");
    printf("          beads. (This one is difficult to play.)\r\n\r\n");
    printf("3    -    HEX NETWORK: Imagine a hex tile floor. Take a rectangle with 20 points\r\n");
    printf("          (intersections) inside (4 x 4). Join the right and left sides to make\r\n");
    printf("          a cylinder. The join the top and bottom to form a torus (doughnut).\r\n");
    printf("          Have fun imagining this one!\r\n\r\n");
    printf("Caves 0 - 3 are 'regular' in the sense that each room goes to three other\r\n");
    printf("rooms and tunnels allow two-way traffice. Here are some 'irregular' caves:\r\n\r\n");
    printf("4    -    DENDRITE WITH DEGENERACIES: Pull a plant from the ground. The roots\r\n");
    printf("          and branches form a dendrite - i.e. there are no looping paths.\r\n");
    printf("          Degeneracy means a) some rooms connect to themselves and\r\n");
    printf("                           b) some rooms have more than one tunnel to the same\r\n");
    printf("                              other room, e.g. 12 has two tunnels to 13.\r\n\r\n");
    printf("5    -    ONE WAY LATTICE: Here all tunnels go one way only. To return, you must\r\n");
    printf("          go around the cave (about 5 moves).\r\n\r\n");
    printf("Finally, there's the DIY variety...\r\n\r\n");
    printf("6    -    ENTER YOUR OWN CAVE: The computer will ask you the rooms next to each\r\n");
    printf("          room in the cave. For example:\r\n");
    printf("            Room #1 connects to ...? 2,3,4 -- (your reply of 2,3,4)\r\n");
    printf("                means room 1 has tunnels going to rooms 2, 3 and 4.\r\n\r\n");
    printf("Happy hunting!\r\n\r\n");
}
