//
//  main.m
//  wumpus
//
//  Created by Petros Loukareas on 16/07/2020.
//  Copyright © 2020 Petros Loukareas. All rights reserved.
//


#import <Foundation/Foundation.h>

int a;
int win;
int t[4];
bool gameOn;
bool sameSetup;
int l[6], m[6];
int cave[60] = { 1, 4, 7, 0, 2, 9, 1, 3, 11, 2, 4, 13, 0, 3, 5, 4,
    6, 14, 5, 7, 16, 0, 6, 8, 7, 9, 17, 1, 8, 10, 9, 11,
    18, 2, 10, 12, 11, 13, 19, 3, 12, 14, 5, 13, 15, 14,
    16, 19, 6, 15, 7, 8, 16, 18, 10, 17, 19, 12, 15, 18 };

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        // Method declarations
        bool arrayContainsValue(int *array, int value, int elements);
        void setUpNewGame(void);
        NSString* getInput(int numberOfCharsReturned, bool suppressPrompt);
        void printHelp(void);
        
        // Game related routines
        void moveWumpus(void);
        bool isPlayerInSameRoomAsWumpus(void);
        int shootArrow(void);
        
        // Scratch variables
        gameOn = true;
        sameSetup = false;
        
        // Print credits?
        if (argv[1] != NULL) {
            NSString *arg = [NSString stringWithUTF8String:argv[1]];
            if ([arg isEqualToString:@"--credits"]) {
                
                // Print credits and exit
                printf("\r\nCREDITS FOR WUMPUS                        ObjC 1.01 Build 3\r\n");
                printf("-----------------------------------------------------------\r\n");
                printf("Original concept and program: Gregory Yob            (1973)\r\n");
                printf("   (published inPeople's Computer Company newsletter, 1974)\r\n");
                printf("Objective-C port:             Kinoko House           (2020)\r\n");
                printf("\r\n");
                printf("The source to this port is released into the public domain:\r\n");
                printf("https://bitbucket.org/kinokohouse/wumpus\r\n\r\n");
            }
            return 0;
        }
        
        // Game start
        printf("\r\n                                 WUMPUS\r\n");
        printf("               CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY\r\n\r\n\r\n");
        printf("Welcome to 'Hunt the Wumpus'!\r\nNeed instructions (y/N)? ");
        NSString *n = getInput(1, true);
        if ([n isEqualToString:@"y"]) printHelp();
        
        // Game loop
        while (gameOn) {
            if (!sameSetup) {
                setUpNewGame();
            }
            a = 5;
            printf("\r\nHunt the Wumpus!\r\n");
            win = 0;
            while (win == 0) {
                printf("\r\n");
                t[0] = l[0];
                bool tflag = false;
                for (int i = 1; i < 4; i++) {
                    t[i] = cave[(t[0] * 3) + i - 1];
                    if (l[1] == t[i]) {
                        printf("I smell a Wumpus!\r\n");
                        tflag = true;
                    }
                    if (l[2] == t[i]) {
                        printf("I feel a draft!\r\n");
                        tflag = true;
                    }
                    if (l[3] == t[i]) {
                        printf("I feel a draft!\r\n");
                        tflag = true;
                    }
                    if (l[4] == t[i]) {
                        printf("Bats nearby!\r\n");
                        tflag = true;
                    }
                    if (l[5] == t[i]) {
                        printf("Bats nearby!\r\n");
                        tflag = true;
                    }
                }
                if (tflag) printf("\r\n");
                printf("You are in room %d.\r\n",t[0] + 1);
                printf("Tunnels lead to rooms %d, %d and %d.\r\n\r\n", t[1] + 1, t[2] + 1, t[3] + 1);
                NSString *answer = @"";
                while ([answer isNotEqualTo:@"s"] && [answer isNotEqualTo:@"m"]) {
                    printf("Shoot or move (s/m)? ");
                    answer = getInput(1, true);
                }
                if ([answer isEqualToString:@"s"]) {
                    int result = shootArrow();
                    if (result == 0) {
                        printf("Missed...\r\n");
                        moveWumpus();
                        if (isPlayerInSameRoomAsWumpus()) {
                            printf("Tsk tsk tsk -- Wumpus got you!\r\n");
                            win = -1;
                        } else {
                            a--;
                            if (a == 0) {
                                printf("Out of arrows!\r\n");
                                win = -1;
                            }
                        }
                    } else {
                        if (result == -1) printf("OUCH! Arrow got you!\r\n");
                        win = result;
                    }
                } else if ([answer isEqualToString:@"m"]) {
                    int newRoom = -1;
                    while (newRoom != t[1] + 1 && newRoom != t[2] + 1 && newRoom != t[3] + 1) {
                        printf("Where to? ");
                        newRoom = [getInput(0, true) intValue];
                        if (newRoom != t[1] + 1 && newRoom != t[2] + 1 && newRoom != t[3] + 1) {
                            if (newRoom < 1 || newRoom > 20) {
                                printf("No such room - try another one.");
                            } else {
                                printf("Not possible - please choose between %d, %d and %d.\r\n", t[1] + 1, t[2] + 1, t[3] + 1);
                            }
                        }
                    }
                    l[0] = newRoom - 1;
                checkHazards:
                    if (l[0] == l[1]) {
                        printf("Oops! Bumped into a Wumpus!\r\n");
                        moveWumpus();
                        if (isPlayerInSameRoomAsWumpus()) {
                            printf("Tsk tsk tsk -- Wumpus got you!\r\n");
                            win = -1;
                        }
                    }
                    if (l[0] == l[2] || l[0] == l[3]) {
                        printf("YYYIIIIEEEE . . . Fell in pit.\r\n");
                        win = -1;
                    }
                    if (l[0] == l[4]) {
                        printf("ZAP -- Super bat snatch! Elsewhereville for you!\r\n");
                        l[0] = arc4random_uniform(19);
                        goto checkHazards;
                    }
                    if (l[0] == l[5]) {
                        printf("ZAP -- Super bat snatch! Elsewhereville for you!\r\n");
                        l[0] = arc4random_uniform(19);
                        goto checkHazards;
                    }
                }
            }
            if (win == -1) {
                printf("Ha ha ha - You lose!");
                
            } else {
                printf("Aha! you got the Wumpus!\r\n");
                printf("Hee hee hee - The Wumpus'll getcha next time!!");
            }
            printf("\r\n\r\nAnother game (Y/n)? ");
            NSString *aw = getInput(1, true);
            if ([aw isEqualToString:@"y"] || [aw isEqualToString:@""]) {
                gameOn = true;
                printf("Same setup (y/N)? ");
                if ([getInput(1, true) isEqualToString:@"y"]) {
                    sameSetup = true;
                    for (int i = 0; i < 6; i++) {
                        l[i] = m[i];
                    }
                } else {
                    sameSetup = false;
                }
            } else {
                gameOn = false;
            }
        }
    }
    printf("Thanks for playing, see you again!\r\n");
    return 0;
}

bool arrayContainsValue(int *array, int value, int elements) {
    for (int i = 0; i < elements; i++) {
        if (array[i] == value) return true;
    }
    return false;
}

void setUpNewGame() {
    for (int i = 0; i < 6; i++) {
        int v = arc4random_uniform(19);
        while (arrayContainsValue(l, v, i)) {
            v = arc4random_uniform(19);
        }
        l[i] = v;
        m[i] = v;
    }
}

NSString* getInput(int numberOfCharsReturned, bool suppressPrompt) {
    char input[1000] = { };
    if (!suppressPrompt) printf("> ");
    fgets(input, sizeof(input), stdin);
    NSString *i = [[NSString stringWithUTF8String:input] lowercaseString];
    i = [[i componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
    if ([i length] == 0) return @"";
    if (numberOfCharsReturned < 1) {
        return i;
    } else {
        if ([i length] >= numberOfCharsReturned) {
            return [i substringWithRange:NSMakeRange(0, numberOfCharsReturned)];
        } else {
            return i;
        }
    }
}

void moveWumpus() {
    int mvarray[4] = { };
    int wroom = l[1];
    mvarray[0] = wroom;
    for (int j = (wroom * 3); j < ((wroom * 3) + 3); j++) {
        mvarray[j - (wroom * 3) + 1] = cave[j];
    }
    l[1] = mvarray[arc4random_uniform(3)];
}

bool isPlayerInSameRoomAsWumpus() {
    if (l[0] == l[1]) return true;
    return false;
}

int shootArrow() {
    int numberOfRooms = 0;
    int t[3];
    while (numberOfRooms < 1 || numberOfRooms > 5) {
        printf("How many rooms (1 - 5)? ");
        numberOfRooms = [getInput(0, true) intValue];
        if (numberOfRooms > 5) {
            printf("Arrow don't travel *that* far!\r\n");
        }
        if (numberOfRooms < 1) {
            printf("A number between 1 and 5 will do.\r\n");
        }
    }
    int arrows[numberOfRooms];
    int pf;
    for (int i = 0; i < numberOfRooms; i++) {
        pf = 0;
        while (pf < 1 || pf > 20) {
            printf("Room #%d? ", i + 1);
            pf = [getInput(0, true) intValue];
            if (pf < 1 || pf > 20) {
                printf("Illegal room number -- please try again.\r\n");
            }
        }
        pf--;
        if (i > 1) {
            if (pf == arrows[i - 2]) {
                printf("Arrows aren't *that* crooked -- try another room.\r\n");
                i--;
                continue;
            } else {
                arrows[i] = pf;
            }
        } else {
            arrows[i] = pf;
        }
    }
    int l0 = l[0];
    if (l0 == arrows[0]) return -1;
    for (int i = 0; i < numberOfRooms; i++) {
        for (int j = 0; j < 3; j++) {
            t[j] = cave[(l0 * 3) + j];
        }
        int p = -1;
        for (int k = 0; k < 3; k++) {
            if (t[k] == arrows[i]) {
                l0 = t[k];
                p = l0;
                break;
            }
        }
        if (p == -1) {
            l0 = t[arc4random_uniform(2)];
            p = l0;
        }
        if (p == l[0]) {
            return -1;
        }
        if (p == l[1]) {
            return 1;
        }
    }
    return 0;
}

void printHelp() {
    printf("\r\nWelcome to 'Hunt The Wumpus'\r\n\r\n");
    printf("The Wumpus lives in a cave of 20 rooms. Each room has 3 tunnels leading to\r\n");
    printf("other rooms. (Look at a dodecahedron to see how this works - If you don't know\r\n");
    printf("what a dodecahedron is, here's a rough ASCII picture (make sure your terminal\r\n");
    printf("window is the default 80 characters wide):\r\n\r\n");
    printf("            \033[0;31m[0]\033[0m- - - - - - - - ' ' ' ' ' ' ' ' ' ' ' ' ' '\033[0;31m[0]\033[0m\r\n");
    printf("            .   o                                       o   \\\r\n");
    printf("           o      \033[0;31m[0]\033[0m, ....------- -\033[0;31m[0]\033[0m-----------'\033[0;31m[0]\033[0m       &\r\n");
    printf("           .       ,                &o              oo         o\r\n");
    printf("          o       .                  o                &         o\r\n");
    printf("         .        &                 \033[0;31m[0]\033[0m.               o         *\r\n");
    printf("         o        o               o      o,             o         \\\r\n");
    printf("        *         /            ,/           o            o         \\\r\n");
    printf("        o        \033[0;31m[0]\033[0m -.       o               \033[0;31m[0]\033[0m--,&--- \033[0;31m[0]\033[0m         &\r\n");
    printf("       #         o     '-..\033[0;31m[0]\033[0m                 &#          o          o\r\n");
    printf("       o         ,           .                                ,        \\\r\n");
    printf("      &         o             (                &               o        .\r\n");
    printf("      /        .               &              o                  .        .\r\n");
    printf("     &        oo*              \033[0;31m[0]\033[0m ....... \033[0;31m[0]\033[0m                  \033[0;31m[0]\033[0m       #.\r\n");
    printf("     *    *o# \033[0;31m[0]\033[0m             .,.             #(               .o          \033[0;31m[0]\033[0m\r\n");
    printf("   \033[0;31m[0]\033[0m#            o        *                    ,*         o             o\r\n");
    printf("         o            *   &                         /o  o             //\r\n");
    printf("             o         \033[0;31m[0]\033[0m                         .\033[0;31m[0]\033[0m            o\r\n");
    printf("                &/           o                  o              .o\r\n");
    printf("                    o            &,         &.              &.\r\n");
    printf("                        o            *\033[0;31m[0]\033[0m,               o\r\n");
    printf("                           &           &             &.\r\n");
    printf("                               o        .         o\r\n");
    printf("                                  .&    o     (/\r\n");
    printf("                                      & oo o\r\n");
    printf("                                        \033[0;31m[0]\033[0m\r\n\r\n");
    printf("Well, I did say it was rough. The red nodes are the rooms, the 'lines' show\r\n");
    printf("the tunnels that interconnect them.\r\n\r\n");
    printf("     HAZARDS:\r\n");
    printf("Bottomless pits - Two rooms have bottomless pits in them. If you go there,\r\n");
    printf("     you fall into the pit (and lose!)\r\n");
    printf("Superbats - Two other rooms have superbats. If you go there, a bat grabs you\r\n");
    printf("     and takes you to some other room at random (which might be troublesome).\r\n\r\n");
    printf("     WUMPUS:\r\n");
    printf("The Wumpus is not bothered by the hazards (he has sucker feet and is too big\r\n");
    printf("for a bat to lift). Usually he is asleep. Two things that wake him up: your\r\n");
    printf("entering his room or your shooting an arrow.\r\n");
    printf("     If the Wumpus wakes, he moves (p = 0.75) one room or stays still\r\n");
    printf("(p = 0.25). After that, if he is where you are, he eats you up (and you lose!)\r\n\r\n");
    printf("     YOU:\r\n");
    printf("Each turn you may move or shoot a crooked arrow.\r\n");
    printf("Moving - You can go one room (thru one tunnel).\r\n");
    printf("Arrows - You have 5 arrows. You lose when you run out. Each arrow can go from\r\n");
    printf("     1 to 5 rooms. You aim by telling the computer the room numbers you want\r\n");
    printf("     the arrow to go. If the arrow can't go that way (i.e. no tunnel) it moves\r\n");
    printf("     at random to the next room.\r\n");
    printf("     If the arrow hits the Wumpus, you win.\r\n");
    printf("     If the arrow hits you, you lose.\r\n\r\n");
    printf("     WARNINGS:\r\n");
    printf("When you are one room away from Wumpus or hazard, the computer says:\r\n");
    printf("     WUMPUS - 'I smell a WUMPUS'\r\n");
    printf("     BAT    - 'Bats nearby'\r\n");
    printf("     PIT    - 'I feel a draft'\r\n\r\n");
}
