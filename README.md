# Wumpus Trilogy #

Hunt the Wumpus! 

These are Objective-C ports of 'Hunt the Wumpus' and its sequels 'Wumpus II' and 'Wumpus III' (also known as 'wump3' or 'Super Wumpus'), originally dreamt up, designed and programmed by the talented and creative [Gregory Yob](https://en.wikipedia.org/wiki/Gregory_Yob) (a.k.a. Hara Ra and later also as Gregory H. Coresun), as originally published in Creative Computing in 1973 (Hunt the Wumpus) and 1977 (Wumpus II), and in 1974 in a 'Games Special' newsletter of the People's Computer Company (Wumpus III).

* 'Hunt the Wumpus' has a cave network that looks like a flattened dodecahedron - see the in-game instructions for that game for a rough ASCII picture of what that looks like. 
* The sequel 'Wumpus II' builds on the original game by adding 5 more cave topologies, and by giving you the opportunity to build your own cave.
* 'Wumpus III' goes back to the basics insofar as the cave is concerned, but adds one hazard (airborne termites called 'tumaeros') and all hazards (including the Wumpus, who is prone to sleep-walking!) can move around the cave.

For 'Wumpus III', you can also start the game with the options `-kr` or `--kinokorules` to change the gameplay a bit: hazards cannot relocate in the first round (to make things a teensy weensy bit fairer) and there's 50% chance per game of an extra arrow lying about somewheres that you can pick up and put in your quiver. However, shooting your arrow into a room with the tumaero swarm in it will result in it being eaten and not traveling further, and you'll lose an arrow if you draw your bow in the room with the tumaeros in it (so no quick aim at the Wumpus from that room anymore).

You can view the credits for all programs by using the `--credits` option in the terminal.

In the spirit of creative computing in the 70's, the code is released under a modern public domain [unlicense](https://unlicense.org).

Ready-to-go binaries for 10.7 and up are in the `Downloads` section; install in `/usr/local/bin` since that's where stuff like this belongs.

## Building

Project is for Xcode 10 and up, but you'd be able to build the one-filer `main.m` for each version from the command line if need be.

### Version History

#### Wumpus & Wumpus II
**1.01 (build 3, current version):**

* Small fix for a big bug - hazards could be placed outside of the game in a mythical room 21 (this included you and the Wumpus by the way!)
* `Hunt the Wumpus!`

#### Wumpus III
**1.1 (build 2, current version):**

* Made Kinoko House Rules additions
* Removed some debug logging that was left in

**1.0 (build 1):**

* Initial release
* `Hunt the Wumpus, too!`



